export const breakpoints = ['320px', '375px', '414px', '768px', '1024px', '1330px', '1440px', '1980px', '2560px'];

export const MediaQueries = {
    iphone5: `@media(min-width: ${breakpoints[0]})`, //320
    iphone6: `@media(min-width: ${breakpoints[1]})`, //375
    iphoneplus: `@media(min-width: ${breakpoints[2]})`, //414
    ipad: `@media(min-width: ${breakpoints[3]})`, //768
    ipadpro: `@media(min-width: ${breakpoints[4]})`, //1024
    ipadpro2: `@media(min-width: ${breakpoints[5]})`, //1330
    laptop: `@media(min-width: ${breakpoints[6]})`, //1440
    fullhd: `@media(min-width: ${breakpoints[7]})`, // 1980
    fourk: `@media(min-width: ${breakpoints[8]})`, // 2560
}