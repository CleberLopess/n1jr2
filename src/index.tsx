import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

import {Layout} from '../src/Layout'
import {Header} from './sections/header'
import {HeroBanner} from './sections/heroBanner'
import {CardBanner} from './sections/cardBanner'
import {ProductDestac} from './sections/productDestac'
import {Footer} from './sections/footer'

ReactDOM.render(
  <React.StrictMode>
    <Layout>
      <Header/>
      <HeroBanner/>
      <CardBanner/>
      <ProductDestac/>
      <Footer/>
    </Layout>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
