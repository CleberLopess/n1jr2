import React from 'react'
import * as S from './styles'
import burguer from '../../images/svgs/icon_hamburguer.svg'
import burguerW from '../../images/svgs/icon_hamburguer-W.svg'
import paper from '../../images/svgs/paper-plane.svg'
import paperW from '../../images/svgs/paper-plane-W.svg'
import search from '../../images/svgs/search-solid.svg'
import searchW from '../../images/svgs/search-solid-W.svg'
import bag from '../../images/svgs/shopping-bag-solid.svg'
import bagW from '../../images/svgs/shopping-bag-solid-W.svg'
import logo from '../../images/svgs/logo.svg'

export const Header = () => {
   // const [modal, setModal] = useState(false)

    return(
        <S.Wrapper>
            <section className='section'>
                <div className='div01'>
                    <button className='buttonMenu'>
                        <img className='burguer' src={burguerW}/>
                    </button>
                    <img className='logo' src={logo}/>
                </div>
                <div  className='div02'>
                    <button className='buttonShare'>
                        <img src={paperW}/>
                    </button>
                    <button className='buttonWhere'>
                        <img src={searchW}/>
                    </button>
                    <button className='buttonBag'>
                        <img src={bagW}/>
                    </button>
                </div>
            </section>
        </S.Wrapper>
    )
}