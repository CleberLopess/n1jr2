import styled from 'styled-components'

export const Wrapper = styled.article`
background: transparent;
height: 60px;
display: flex;
justify-content: center;
align-items: center;
position: absolute;
z-index: 1;
width: 100%;
background: rgba(0, 0, 0, 0.65);


.section{
    display: flex;
    justify-content: space-between;
}

.div01{
    width: 50%;
    display: flex;
}

.buttonMenu, .buttonShare, .buttonWhere, .buttonBag{
    background: none;
    border: none;
    cursor: pointer;
}

.buttonMenu{
    margin-right: 20px;
}

`