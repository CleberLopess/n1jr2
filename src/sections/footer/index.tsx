import React from 'react'
import * as S from './styles'
import logoFooter from 'images/svgs/logoFooter.svg'


export const Footer = () => {
    return(
        <S.Wrapper>
                <div className='divImg'>
                    <img className='img' src={logoFooter} alt='logo no rodapé' />;
                </div>
                <div className='divDesc'>
                    <p className='desc'>Agência N1 - Todos os direitos reservados</p>
                </div>
        </S.Wrapper>
    )
} 