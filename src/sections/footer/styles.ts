import styled from 'styled-components'
import {MediaQueries} from 'utils/breakpoints'

export const Wrapper = styled.article`
width: 100%;
height: 68px;
display: flex;
align-items: center;
margin-top: 90px !important;

.divImg{
    width: 79px;
    height: 100%;
    background: #084154;
    display: flex;
}

.img{
    width: 43px;
    height: 28px;
    margin: auto;
}

.divDesc{
    width: 100%;
    height: 100%;
    background: #3EC6E0;
    display: flex;
    align-items: center;
    
}

.desc{
    font-family: Roboto;
    font-size: 12px;
    font-style: normal;
    font-weight: 400;
    line-height: 14px;
    letter-spacing: 0em;
    text-align: left;
    color: #ffffff;
    margin-left: 20px;
}

${MediaQueries.ipadpro2}{
    height: 50px;
    margin-top: 171px !important;

    .divImg{
        width: 547px;
    }

    .img{
        width: 43px;
        height: 28px;
        margin: auto 44px auto auto;
    }

    .desc{
        font-size: 14px;
        line-height: 16px;

    }
}

`