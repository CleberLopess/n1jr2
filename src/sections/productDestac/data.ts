import cyberpunk from '../../images/img/cyberpunk.png'
import DK from '../../images/img/DK.png'
import outriders from '../../images/img/outriders.png'
import dotsgroup from '../../images/svgs/dotsGroup.svg'


const data = [

    {
        img:outriders,
        namegame:'Outriders',
        price: 'R$ 200,00'
    },
    {
        img:cyberpunk,
        namegame:'CYBERPUNK 2077',
        price: 'R$ 200,00'
    },
    {
        img:DK,
        namegame:'Donkey Kong Countru Tropical Freeze',
        price: 'R$ 200,00'
    }
]

const dots = dotsgroup

export default {data, dots}