import React, {useState} from 'react'
import * as S from './styles'
import data from './data'
import {Modal} from 'sections/modal'

import Slider from "react-slick";
import Left from '../../images/svgs/angle-left-solid.svg'
import Right from '../../images/svgs/angle-right-solid.svg'

import useWindowSize from 'utils/windows'



export const ProductDestac = () => {
    const [sale, setsale] = useState('Comprar')
    const [onModal, setOnModal] = useState<boolean>();
    const {width} = useWindowSize()

    const closeModal = () => (
        setsale('Comprado'), 
        setOnModal(true)
    )

    
    const settings = {
        className: 'slider',
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow/>,
        prevArrow: <SamplePrevArrow/>
    };  

    function SampleNextArrow(props: any) {
        const { onClick } = props;
        return (
        <button className='buttonNext' onClick={onClick}>
            <img src={Right}/> 
        </button>
        );
      }

      function SamplePrevArrow(props: any) {
        const { onClick } = props;
        return (
          <button className='buttonPrev' onClick={onClick}>
              <img src={Left}/> 
          </button>
        );
      }

    return(
        <>
        <S.Wrapper>
        {onModal ? <Modal closeModal={onModal} /> : null}
            <section>
                <>
                <div className='title'>
                    <img className='dotsGroup' src={data.dots} alt="dots" />
                    <p className='products'>Produtos em destaque</p>
                </div>
                <div className='map'>
                {width < 1330 ? 
                <Slider {...settings}>
                {data.data.map((item, n) => (
                    <div className={`allDiv`+ n}>
                        <div className='divImg'>
                            <img className='imagemGame' src={item.img} alt="imagem do jogo" />
                        </div>
                        <div className='allDesc'>
                            <p className='nameGame'>{item.namegame}</p>
                            <p className='priceGame'>{item.price}</p>
                            <button className='buttonSale' onClick={() => closeModal()}>{sale}</button>
                        </div>
                    </div>
                ))}
                </Slider>
                : 
                <>
                {data.data.map((item, n) => (
                    <div className={`allDiv`+ n}>
                        <div className='divImg'>
                            <img className='imagemGame' src={item.img} alt="imagem do jogo" />
                        </div>
                        <div className='allDesc'>
                            <p className='nameGame'>{item.namegame}</p>
                            <p className='priceGame'>{item.price}</p>
                            <button className='buttonSale' onClick={() => closeModal()} >{sale}</button>
                        </div>
                    </div>
                ))}
                </>
                }
            </div>
            </>
            </section>
        </S.Wrapper>
        </>
    )
}