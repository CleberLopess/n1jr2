import styled from 'styled-components'
import {MediaQueries} from 'utils/breakpoints'
import mario from 'images/svgs/mario.svg'


export const Wrapper = styled.article`

.title{
    display: flex;
    align-items: center;
    margin-top: 42px;
    margin-bottom: 31px;
}

.dotsGroup{
    width: 18px;
}

.products{
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 300;
    line-height:21px;
    letter-spacing: 0em;
    color: #084154;
    margin-left: 22px;
}

.allDiv0, .allDiv1, .allDiv2{
    width: 236px !important;
    border-radius: 10px;
    margin: auto;
    display: block;
    background: #FFFFFF;
    box-shadow: 0px 4px 20px 7px rgba(0, 0, 0, 0.07);
    border-radius: 10px;
    

}

.divImg{
    height: 279px;
    width: 236px;
    display: flex;
    align-items: flex-end;
}

.imagemGame{
    height: 244px;
    width: 100%;
}

.allDesc{
    height: 141px;
    background: #F5F5F5;
    border-radius: 0px 0px 10px 10px;
    padding: 16px 24px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
}

.nameGame{
    font-family: Roboto;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: 0em;
    text-align: left;
    color: #084154;
}

.priceGame{
    font-family: Roboto;
    font-size: 18px;
    font-style: normal;
    font-weight: 900;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    color: #084154;

}

.buttonSale{
    height: 35px;
    background: #3EC6E0;
    border-radius: 5px;
    border: none;
    color: #FFFFFF;
    cursor: pointer;
    
    &:hover{
        background: #084154;
    }

    &:focus{
        background: #084154;
        content: '';
        background-image: url(${mario});
        background-repeat: no-repeat;
        background-position: right;
        background-position-y: 0px;
        position: relative;
    }
}

.buttonPrev, .buttonNext{
    border: none;
    background: transparent;
    cursor: pointer;
}

.slick-list{
    width: 75%;
}

.slick-slider{
    display: flex;
    align-items: center;
    justify-content: center;

    div:nth-child(1){
        display: flex;
    }

}

${MediaQueries.ipadpro}{
    .dotsGroup{
        width: 25px;
    }

    .products{
        font-size: 25px;
    }

    .nameGame{
        font-size: 18px;
    }
}

${MediaQueries.ipadpro2}{

    .title{
        margin-bottom: 70px;
    }

    .map{
        display: flex;
    }

    .allDiv0, .allDiv1, .allDiv2{
        width: 290px !important;
    }

    .divImg{
        margin: auto;
    }

    .buttonSale{
        font-size: 20px;
    }
}

${MediaQueries.laptop}{
    .title{
        margin-top: -80px;
    }
}

${MediaQueries.fullhd}{

    .dotsGroup{
        width: 28px;
    }

    .products{
        font-size: 36px;
        line-height: 42px;
    }

    .allDiv0, .allDiv1, .allDiv2{
        width: 350px !important;
    }

    .divImg{
        height: 414px;
        margin: 0;
    }

    .imagemGame{
        height: 362px;
        width: 350px;
    }

    .buttonSale{
        height: 52px;
    }

}
`