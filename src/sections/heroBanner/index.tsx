import React from 'react'
import * as S from './styles'
import Slider from "react-slick";
import LeftW from '../../images/svgs/angle-left-solidW.svg'
import RightW from '../../images/svgs/angle-right-solidW.svg'

export const HeroBanner = () => {
    const settings = {
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        nextArrow: <SampleNextArrow/>,
        prevArrow: <SamplePrevArrow/>
    };   

    function SampleNextArrow(props: any) {
        const { onClick } = props;
        return (
        <button className='buttonNext' onClick={onClick}>
            <img src={RightW}/> 
        </button>
        );
      }

      function SamplePrevArrow(props: any) {
        const { onClick } = props;
        return (
          <button className='buttonPrev' onClick={onClick}>
              <img src={LeftW}/> 
          </button>
        );
      }

    return(
        <S.Wrapper>   
        <div className='carrousel'>
            <Slider {...settings}>
            <div className='allItem'>
                <div className='item01'>
                    <section className='section'>
                        <div className='bgSection'>
                            <section className='sectionsDesc'>
                                <h1 className='gameName'>Mortal Kombat</h1>
                                <h2 className='price01'>R$ 299 <b className='price02'>,90</b></h2>
                                <p className= 'desc'>Mortal Kombat X combina uma apresentação cinemática única com uma jogabilidade totalmente nova. Os jogadores podem escolher pela primeira vez diversas variantes de cada personagem, afetando tanto a estratégia como o estilo de luta.</p>
                            </section>
                        </div>
                    </section>
                </div>
                <div className='divBlue'>
                    <p  className='nameGameCarrousel'>Mortal Kombat</p>
                    <div className='divArrow'><p  className='numbeCarrousel'>1 / 2</p></div>
                </div>
            </div>
            <div className='allItem'>
                <div className='item02'>
                    <section className='section'>
                        <div className='bgSection'>
                            <section className='sectionsDesc'>
                                <h1 className='gameName'>Red Dead Redemption</h1>
                                <h2 className='price01'>R$ 179 <b className='price02'>,80</b></h2>
                                <p className= 'desc'>Red Dead Redemption é um jogo de faroeste de ação-aventura desenvolvido pela Rockstar San Diego e apresentado como o sucessor espiritual de Red Dead Revolver.</p>
                            </section>
                        </div>
                    </section>
                </div>
                <div className='divBlue'>
                    <p className='nameGameCarrousel'>Red Dead Redemption</p>
                    <div className='divArrow'><p  className='numbeCarrousel'>2 / 2</p></div>
                </div>
            </div>
            </Slider>   
            </div>
        </S.Wrapper>
    )
}