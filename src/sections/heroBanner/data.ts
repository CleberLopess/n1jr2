import bannerDSK from '../../images/img/principal_banner_desktop.jpg'
import bannerDSK2 from '../../images/img/principal_banner_desktop_02.jpg'
import bannerMob from '../../images/img/principal_banner_mobile.jpg'
import bannerMob2 from '../../images/img/principal_banner_mobile_02.jpg'


export const data = [
    {
        name: 'Mortal Kombat',
        imgMob: bannerMob,
        imgDsk: bannerDSK,  
        desc: 'Mortal Kombat X combina uma apresentação cinemática única com uma jogabilidade totalmente nova. Os jogadores podem escolher pela primeira vez diversas variantes de cada personagem, afetando tanto a estratégia como o estilo de luta.',
        price: [`R$ 299`,`,90`]
    },
    {
        name: 'Red Dead Redemption',
        imgMob: bannerMob2,
        imgDsk: bannerDSK2 ,
        desc: 'Red Dead Redemption é um jogo de faroeste de ação-aventura desenvolvido pela Rockstar San Diego e apresentado como o sucessor espiritual de Red Dead Revolver',
        price: [`R$ 179`,`,45`]
    }
]