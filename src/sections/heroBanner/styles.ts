import styled from 'styled-components'
import {MediaQueries} from '../../utils/breakpoints'
import bannerDSK from '../../images/img/principal_banner_desktop.jpg'
import bannerDSK2 from '../../images/img/principal_banner_desktop_02.jpg'
import bannerMob from '../../images/img/principal_banner_mobile.jpg'
import bannerMob2 from '../../images/img/principal_banner_mobile_02.jpg'

export const Wrapper = styled.article`
width: 100vw;

.item01{
    background-image: url(${bannerMob});
}

.item02{
    background-image: url(${bannerMob2});
}

.item01, .item02{
    display: flex !important;
    align-items: flex-end;
    justify-content: center;
    background-size: cover;
    background-repeat: no-repeat;
    height: 60vh;
    width: 100vw;
}

.section{
    width: 100% ;
    margin: 0 ;
}

.bgSection{
    height: 160px;
    width: 100%;
    text-align: end;
    z-index: 1;
    color: azure;
    padding: 14px 0;
    background: rgba(0, 0, 0, 0.65);
}

.gameName{
    font-size: 26px;
    font-style: normal;
    font-weight: 900;
    line-height: 30px;
    letter-spacing: 0em;
    
}

.price01{
    display: flex;
    align-items: center;
    justify-content: flex-end;
    font-size: 48px;
    font-style: normal;
    font-weight: 900;
    line-height: 56px;
    letter-spacing: 0em;
    text-align: left;
    color: #3EC6E0;
    
}

.price02{
    font-size: 28px;
    font-style: normal;
    font-weight: 900;
    line-height: 33px;
    letter-spacing: 0em;
    text-align: left;
    
}

.desc{
    font-size: 12px;
    font-style: normal;
    font-weight: 400;
    line-height: 19px;
    letter-spacing: 0em;
    text-align: right;
    
}

.divBlue{
    background: #3EC6E0;
    border-radius: 0px 0px 0px 10px;
    height: 52px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-left: 10px;
}

.nameGameCarrousel{
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: 0em;
    color: #ffffff;
    width: 100%;

}

.divArrow{
    background: #084154;
    height: 52px;
    width: 140px;
    display: flex;
    align-items: center;
    color: #ffffff;
    padding-left: 18px;
}

.buttonNext, .buttonPrev{
    background-color: transparent;
    border: none;
    position: absolute;
    display: flex;
    top: 91%;
    z-index: 1;
}

.buttonNext{
    left: 92%;
}

.buttonPrev{
    left: 82%;
}

${MediaQueries.iphone6}{

    .buttonNext, .buttonPrev{
        top: 92%;
    }
}

${MediaQueries.iphoneplus}{

    .buttonNext, .buttonPrev{
    top: 93%;
}

.buttonPrev{
    left: 84%;
}
}

${MediaQueries.ipad}{

    .divBlue{
        padding-left: 30px;
    }

    .gameName{
        font-size: 35px;
    }

    .nameGameCarrousel{
        font-size: 20px;
        width: 100%;
        display: flex;
        align-items: center;

        &:after{
            content: '';
            border: 1px solid #FFFFFF;
            width: 50%;
            margin: auto;
        }
    }

    .buttonNext, .buttonPrev{
        top: 94.5%;;
    }

    .buttonNext {
        left: 95%;
    }

    .buttonPrev{
        left: 90%;
    }
}

${MediaQueries.ipadpro}{

    .item01{
        background-image: url(${bannerDSK});
    }

    .item02{
        background-image: url(${bannerDSK2});
    }

    .gameName{
        font-size: 40px;
    }

    .desc{
        font-size: 17px;
    }

    .buttonNext, .buttonPrev{
        top: 93.5%;;
    }

    .buttonNext {
        left: 96%;
    }

    .buttonPrev{
        left: 92%;
    }

}

${MediaQueries.ipadpro2}{

    .buttonNext, .buttonPrev{
        top: 91.5%;
    }

    .buttonNext {
        left: 97%
    }

    .buttonPrev{
        left: 94%;
    }

}

${MediaQueries.laptop}{

    .item01, .item02{
        height: 75vh;
        align-items: center;
    }
    
    .section{
        width: 75%;
        margin: auto ;
        display: flex;
        justify-content: flex-end;
    }

    .bgSection{
        height: 327px;
        width: 384px;
        padding-inline: 20px;
    }

    .sectionsDesc{
        width: 100% !important;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
    }

    .gameName{
        font-size: 46px;  
    }

    .price01{
        font-size: 70px;
        align-items: end;
    }

    .desc{
        font-size: 16px;
        line-height: 24px;
    }

    .divBlue{
        transform: rotate(90deg);
        width: 384px;
        height: 63px;
        position: absolute;
        top: 40%;
        left: 84%;
        border-radius: 0px 0px 10px 0px;
    }

    .divArrow{
        height: 100%;
        border-radius: 0px 0px 10px 0px;
    }

    .nameGameCarrousel{
        &:after{
            width: 25%;
        }
    }

    .numbeCarrousel{
        transform: rotate(-90deg);
    }

    .buttonNext, .buttonPrev{
        top: 70%;
        img{
            width: 10px;
        }
    }

    .buttonNext {
        left: 98.5%
    }

    .buttonPrev{
        left: 96.5%;
    }
}

${MediaQueries.fullhd}{

    .divBlue{
        height: 80px;
        left: 88%;
    }

    .buttonNext, .buttonPrev{
        img{
            width: 14px;
        }
    }

    .buttonNext {
        left: 98.8%
    }

    .buttonPrev{
        left: 96.8%;
    }

}

${MediaQueries.fourk}{
    .section{
        width: 70%;
    }

    .divBlue{
        height: 90px;
        left: 90%;
    }

}

`