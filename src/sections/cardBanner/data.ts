import zelda from '../../images/img/zelda_banner.jpg'
import sekiro from '../../images/img/sekiro_banner.jpg'

const data = [
    {
        img:zelda,
        namegame:'The Legend of Zelda - Breath of th wild'
    },
    {
        img:sekiro,
        namegame:'SEKIRO - Shadows die twice'
    }
]

export default data