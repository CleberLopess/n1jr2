import styled from 'styled-components'
import {MediaQueries} from 'utils/breakpoints'

export const Wrapper = styled.article`

.section{
    margin-top: 25px;
}

.item0{
    margin-bottom: 10px;
}

.imageGame{
    width: 100%;
}

.divBlu{
    background: #3EC6E0;
    margin-top: -5px;
    height: 31px;
    color: #084154;
    display: flex;
    align-items: center;
    border-radius: 5px;
    border-left: solid 5px #084154;
}

.nameGame{
    margin-left: 10px;
    white-space: nowrap;
    display: flex;
    width: 100%;

    &::after{
        content: '';
        border: 1px solid #084154;
        margin: auto 15px;
        width: inherit;
        height: 0px;
    }
}

.name{
    font-size: 12px;
    font-family: Roboto;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 19px;
    letter-spacing: 0em;
    width: auto;
}


${MediaQueries.ipad}{

    .divBlu{
        height: 35px;
    }

    .name{
    font-size: 15px;
    }   

}

${MediaQueries.ipadpro}{
    .section{
        display: flex;
        justify-content: space-between;
    }

    .item0, .item1{
        width: 49%;
    }
}

${MediaQueries.laptop}{
    .section{
        position: relative;
        top: -150px;
    }
}


`
