import React from 'react'
import * as S from './styles'
import data from './data'

export const CardBanner = () => {
    return(
        <S.Wrapper>
            <section className='section'>
                {data.map((item, n) => {
                    return (
                        <div className={`item`+ n}>
                            <img className='imageGame' src={item.img} alt="imagem do jogo" />
                            <div className='divBlu'>
                                <div className= 'nameGame'>
                                    <p className='name'>{item.namegame}</p>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </section>
        </S.Wrapper>
    )
}


