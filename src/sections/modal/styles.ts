import styled from 'styled-components'

export const Wrapper = styled.article`
position: absolute;
background: rgba(8, 65, 84, 0.62);
width: 100vw;
height: 83vh;

.allDiv{
    margin: auto;
    height: 500px;
    width: 400px;
    background-color: #fff;
    border-radius: 5px;

}

.button{
    background: none;
    border: none;
    border-radius: 50%;
    margin: 10px 0px 0 auto;
    display: flex;
}

.divdesc{
    display: flex;
    justify-content: center;
}

.line{
    border: 1px solid #084154;
    width: 100%;
    height: 0px;
    align-items: center;
    position: relative;
    z-index: 1;
    top: 40px;
}

.desc{
    background-color: #fff;
    text-align: center;
    position: relative;
    z-index: 2;
    width: 247px;
    margin: auto;

    font-family: Roboto;
    font-size: 30px;
    font-style: normal;
    font-weight: 300;
    line-height: 35px;
    letter-spacing: 0em;
    text-align: center;

}

.divMario{
    margin: auto auto auto 0;
    height: 76%;
    display: flex;
}

.imgMario{
    margin: auto auto 0 auto;
    display: flex;
}

`


