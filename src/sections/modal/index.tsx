import React, { useState }from 'react'
import * as S from './styles'
import marioModal from 'images/svgs/marioModal.svg'
import close from 'images/svgs/close_btn.svg'

type Props = {
    closeModal?: boolean
}

export const Modal = ({closeModal}:Props) => {

    const [Modal, setModal] = useState(closeModal);

    return(
        <S.Wrapper>
            {Modal ? (
            <div className='allDiv'>
                <div className='divButton'>
                    <button className='button' onClick={() => setModal(false)}>
                        <img className='imageButton' src={close} alt='botao de fechar o modal'/>
                    </button>
                </div>
                <div className='divDesc'>
                    <div className='line'/>
                    <p className='desc'>Pedido realizado com sucesso!</p>
                </div>
                <div className='divMario'>
                    <img className='imgMario' src={marioModal} alt='imagem do mario' />
                </div>
            </div>
            ) : null}
        </S.Wrapper>
        
    )
}
