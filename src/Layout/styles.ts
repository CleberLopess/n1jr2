import styled from 'styled-components'
import {MediaQueries} from '../utils/breakpoints'
import {Colors} from '../utils/colors'

export const Wrapper = styled.article`
background-color: ${Colors.background};
font-family: 'Roboto';
width: 100vw;

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	vertical-align: baseline;
}

    section{
        width: 95%;
        margin: auto;

        ${MediaQueries.ipad}{
            width: 90%;
        }

        ${MediaQueries.laptop}{
            width: 75%;
        }

        ${MediaQueries.fullhd}{
            width: 70%;
        }
    }


`