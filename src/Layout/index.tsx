import React, { Component } from "react"
import * as S from './styles'

type Props = {
    children?: React.ReactNode
}

export const Layout =({children}:Props) => {
    return(
        <S.Wrapper>
            {children}
        </S.Wrapper>
    )
}